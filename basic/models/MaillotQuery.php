<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Maillot]].
 *
 * @see Maillot
 */
class MaillotQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Maillot[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Maillot|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
