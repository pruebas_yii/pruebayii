<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ciclista".
 *
 * @property int $dorsal
 * @property string $nombre
 * @property int|null $edad
 * @property string $nomequipo
 *
 * @property Etapa[] $etapas
 * @property Lleva[] $llevas
 * @property Equipo $nomequipo0
 * @property Puerto[] $puertos
 */
class Ciclista extends \yii\db\ActiveRecord
{
    public $nombre_mayusculas;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ciclista';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dorsal', 'nombre', 'nomequipo'], 'required'],
            [['dorsal', 'edad'], 'integer'],
            [['nombre'], 'string', 'max' => 30],
            [['nomequipo'], 'string', 'max' => 25],
            [['dorsal'], 'unique'],
            [['nomequipo'], 'exist', 'skipOnError' => true, 'targetClass' => Equipo::class, 'targetAttribute' => ['nomequipo' => 'nomequipo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'dorsal' => 'Dorsal',
            'nombre' => 'Nombre',
            'edad' => 'Edad',
            'nomequipo' => 'Nomequipo',
        ];
    }

    /**
     * Gets query for [[Etapas]].
     *
     * @return \yii\db\ActiveQuery|EtapaQuery
     */
    public function getEtapas()
    {
        return $this->hasMany(Etapa::class, ['dorsal' => 'dorsal']);
    }

    /**
     * Gets query for [[Llevas]].
     *
     * @return \yii\db\ActiveQuery|LlevaQuery
     */
    public function getLlevas()
    {
        return $this->hasMany(Lleva::class, ['dorsal' => 'dorsal']);
    }

    /**
     * Gets query for [[Nomequipo0]].
     *
     * @return \yii\db\ActiveQuery|EquipoQuery
     */
    public function getNomequipo0()
    {
        return $this->hasOne(Equipo::class, ['nomequipo' => 'nomequipo']);
    }

    /**
     * Gets query for [[Puertos]].
     *
     * @return \yii\db\ActiveQuery|PuertoQuery
     */
    public function getPuertos()
    {
        return $this->hasMany(Puerto::class, ['dorsal' => 'dorsal']);
    }

    /**
     * {@inheritdoc}
     * @return CiclistaQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CiclistaQuery(get_called_class());
    }
}
