<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "equipo".
 *
 * @property string $nomequipo
 * @property string|null $director
 *
 * @property Ciclista[] $ciclistas
 */
class Equipo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'equipo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nomequipo'], 'required'],
            [['nomequipo'], 'string', 'max' => 25],
            [['director'], 'string', 'max' => 30],
            [['nomequipo'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'nomequipo' => 'Nomequipo',
            'director' => 'Director',
        ];
    }

    /**
     * Gets query for [[Ciclistas]].
     *
     * @return \yii\db\ActiveQuery|CiclistaQuery
     */
    public function getCiclistas()
    {
        return $this->hasMany(Ciclista::class, ['nomequipo' => 'nomequipo']);
    }

    /**
     * {@inheritdoc}
     * @return EquipoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new EquipoQuery(get_called_class());
    }
}
