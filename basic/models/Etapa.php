<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "etapa".
 *
 * @property int $numetapa
 * @property int $kms
 * @property string $salida
 * @property string $llegada
 * @property int|null $dorsal
 *
 * @property Maillot[] $códigos
 * @property Ciclista $dorsal0
 * @property Lleva[] $llevas
 * @property Puerto[] $puertos
 */
class Etapa extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'etapa';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['numetapa', 'kms', 'salida', 'llegada'], 'required'],
            [['numetapa', 'kms', 'dorsal'], 'integer'],
            [['salida', 'llegada'], 'string', 'max' => 35],
            [['numetapa'], 'unique'],
            [['dorsal'], 'exist', 'skipOnError' => true, 'targetClass' => Ciclista::class, 'targetAttribute' => ['dorsal' => 'dorsal']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'numetapa' => 'Numetapa',
            'kms' => 'Kms',
            'salida' => 'Salida',
            'llegada' => 'Llegada',
            'dorsal' => 'Dorsal',
        ];
    }

    /**
     * Gets query for [[Códigos]].
     *
     * @return \yii\db\ActiveQuery|MaillotQuery
     */
    public function getCódigos()
    {
        return $this->hasMany(Maillot::class, ['código' => 'código'])->viaTable('lleva', ['numetapa' => 'numetapa']);
    }

    /**
     * Gets query for [[Dorsal0]].
     *
     * @return \yii\db\ActiveQuery|CiclistaQuery
     */
    public function getDorsal0()
    {
        return $this->hasOne(Ciclista::class, ['dorsal' => 'dorsal']);
    }

    /**
     * Gets query for [[Llevas]].
     *
     * @return \yii\db\ActiveQuery|LlevaQuery
     */
    public function getLlevas()
    {
        return $this->hasMany(Lleva::class, ['numetapa' => 'numetapa']);
    }

    /**
     * Gets query for [[Puertos]].
     *
     * @return \yii\db\ActiveQuery|PuertoQuery
     */
    public function getPuertos()
    {
        return $this->hasMany(Puerto::class, ['numetapa' => 'numetapa']);
    }

    /**
     * {@inheritdoc}
     * @return EtapaQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new EtapaQuery(get_called_class());
    }
}
