<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Etapa]].
 *
 * @see Etapa
 */
class EtapaQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Etapa[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Etapa|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
