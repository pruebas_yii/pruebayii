<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\ActiveDataProvider;
use app\models\Ciclista;
use app\models\Etapa;
use yii\data\SqlDataProvider;

use app\models\Puerto;



class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    
    public function actionConsulta1a() {
        $dataProvider = new ActiveDataProvider([
        'query' => Ciclista::find()->select("edad")->distinct()]); // :: Operador de resolución de ámbitos
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 1 Con Active Record",
            "enunciado"=>"Listar las edades de los ciclistas sin repetidos",
            "sql"=>"SELECT DISTINCT edad FROM ciclista",
        ]);
    } 
    
    public function actionConsulta1() {
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT DISTINCT edad FROM ciclista',
        ]);
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 1 con DAO",
            "enunciado"=>"Listar las edades de los ciclistas (sin repetidos)",
            "sql"=>"SELECT DISTINCT edad FROM ciclista",
        ]);
    }

    public function actionConsulta2a() {
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()->select("edad")->where(["nomequipo" => "Artiach"])->distinct()
               ]);

            return $this->render("resultado",[
                "resultados"=>$dataProvider,
                "campos"=>['edad'],
                "titulo"=>"Consulta 2 Con Active Record",
                "enunciado"=>"Listar las edades de los ciclistas del equipo Artiach",
                "sql"=>"SELECT DISTINCT edad FROM ciclista WHERE nomequipo=Artiach",
            ]);
    }

    public function actionConsulta2() {
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT DISTINCT edad FROM ciclista WHERE nomequipo=\'Artiach\'',
        ]);

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 2 con DAO",
            "enunciado"=>"Listar las edades de los ciclistas del equipo Artiach",
            "sql"=>"SELECT DISTINCT edad FROM ciclista WHERE nomequipo='Artiach'",
        ]);
    }
    // listar las edades de los ciclistas de Artiach o de Amore Vita
    public function actionConsulta3a() {
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()->select("edad")->where(["nomequipo"=>["Artiach","Amore Vita"]])->distinct()]);
            
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 3 Con Active Record",
            "enunciado"=>"Listar las edades de los ciclistas de Artiach o de Amore Vita",
            "sql"=>"SELECT DISTINCT edad FROM ciclista WHERE nomequipo='Artiach' OR nomequipo='Amorevita'",
        ]);
    }

    public function actionConsulta3() {
        $dataProvider = new SqlDataProvider([
            'sql' => 'SELECT DISTINCT edad FROM ciclista WHERE nomequipo="Artiach" OR nomequipo="Amore Vita"'
        ]);

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 3 con DAO",
            "enunciado"=>"Listar las edades de los ciclistas de Artiach o Amore Vita",
            "sql"=>"SELECT DISTINCT edad FROM ciclista WHERE nomequipo='Artiach' OR nomequipo='Amorevita'",
        ]);
    }
    // -- (4) listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30
    public function actionConsulta4a() {
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()->select("dorsal")->where(["not between","edad",25,30])->distinct()]);
            
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 4 Con Active Record",
            "enunciado"=>"listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30",
            "sql"=>"SELECT DISTINCT edad FROM ciclista WHERE nomequipo='Artiach' OR nomequipo='Amorevita'",
        ]);
    }

    public function actionConsulta4() {
        $dataProvider = new SqlDataProvider([
            'sql' => 'SELECT DISTINCT dorsal, edad FROM ciclista WHERE edad NOT BETWEEN 25 and 30'
        ]);

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 4 con DAO",
            "enunciado"=>"listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30",
            "sql"=>"SELECT DISTINCT edad FROM ciclista WHERE nomequipo='Artiach' OR nomequipo='Amorevita'",
        ]);
    }

//     listar los dorsales de los ciclistas cuya edad esté entre 28 y 32 y además que sólo sean de Banesto
    public function actionConsulta5a() {
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()->select("dorsal")->where(["and",["between","edad",28,32],["nomequipo"=>"Banesto"]])
        ]);

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 5 Con Active Record",
            "enunciado"=>"listar los dorsales de los ciclistas cuya edad esté entre 28 y 32 y además que sólo sean de Banesto",
            "sql"=>"SELECT DISTINCT edad FROM ciclista WHERE edad BETWEEN 28 AND 32 AND nomequipo='Banesto'",
        ]);
    }

    public function actionConsulta5() {
        $dataProvider = new SqlDataProvider([
            'sql' => "SELECT DISTINCT edad FROM ciclista WHERE edad BETWEEN 28 AND 32 AND nomequipo='Banesto'"
        ]);

        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 5 Con Active Record",
            "enunciado"=>"listar los dorsales de los ciclistas cuya edad esté entre 28 y 32 y además que sólo sean de Banesto",
            "sql"=>"SELECT DISTINCT edad FROM ciclista WHERE edad BETWEEN 28 AND 32 AND nomequipo='Banesto'",
        ]);

    }

// -- (6) Indícame el nombre de los ciclistas que el número de caracteres del nombre sea mayor que 8

public function actionConsulta6a() {
    $dataProvider = new ActiveDataProvider([
        'query' => Ciclista::find()->select("nombre")->where([">","CHAR_LENGTH(nombre)",8])
    ]);

    return $this->render("resultado",[
        "resultados"=>$dataProvider,
        "campos"=>['nombre'],
        "titulo"=>"Consulta 5 Con Active Record",
        "enunciado"=>"Indícame el nombre de los ciclistas que el número de caracteres del nombre sea mayor que 8",
        "sql"=>"SELECT DISTINCT nombre FROM ciclista WHERE CHAR_LENGTH(nombre) > 8",
    ]);
}

public function actionConsulta6() {
    $dataProvider = new SqlDataProvider([
        'sql' => "SELECT DISTINCT edad FROM ciclista WHERE edad BETWEEN 28 AND 32 AND nomequipo='Banesto'"
    ]);

    return $this->render("resultado",[
        "resultados"=>$dataProvider,
        "campos"=>['dorsal'],
        "titulo"=>"Consulta 6 Con DAO",
        "enunciado"=>"listar los dorsales de los ciclistas cuya edad esté entre 28 y 32 y además que sólo sean de Banesto",
        "sql"=>"SELECT DISTINCT edad FROM ciclista WHERE edad BETWEEN 28 AND 32 AND nomequipo='Banesto'",
    ]);

}

// -- (7) lístame el nombre y el dorsal de todos los ciclistas mostrando un campo nuevo denominado nombre mayúsculas que debe mostrar el nombre en mayúsculas
public function actionConsulta7a() {
    $dataProvider = new ActiveDataProvider([
        'query' => Ciclista::find()->select("nombre,dorsal,UPPER(nombre) AS nombre_mayusculas")->distinct()
    ]);

    return $this->render("resultado",[
        "resultados"=>$dataProvider,
        "campos"=>['nombre','dorsal','nombre_mayusculas'],
        "titulo"=>"Consulta 7 con Active Record",
        "enunciado"=>"listar los dorsales de los ciclistas cuya edad esté entre 28 y 32 y además que sólo sean de Banesto",
        "sql"=>"SELECT DISTINCT nombre, dorsal, UPPER(nombre) AS 'Nombre Mayúsculas' FROM ciclistas",
    ]);
}

public function actionConsulta7() {
    $dataProvider = new SqlDataProvider([
        'sql' => "SELECT DISTINCT nombre, dorsal, UPPER(nombre) AS 'Nombre Mayúsculas' FROM ciclistas",
    ]);
    
    return $this->render("resultado",[
        "resultados"=>$dataProvider,
        "campos"=>['nombre','dorsal','Nombre Mayusculas'],
        "título"=>"Consulta 7 con DAO",
        "enunciado"=>"listar los dorsales de los ciclistas cuya edad esté entre 28 y 32 y además que sólo sean de Banesto",
        "sql"=>"SELECT DISTINCT nombre, dorsal, UPPER(nombre) AS 'Nombre Mayúsculas' FROM ciclistas",
    ]);
}

// -- (8) Listar todos los ciclistas que han ganado el maillot MGE (amarillo) en alguna etapa
public function actionConsulta8a() {
    $dataProvider = new ActiveDataProvider([
        'query'=> Etapa::find()->select("dorsal")->distinct(),
    ]);

    return $this->render("resultado",[
        "resultados"=>$dataProvider,
        "campos"=>['dorsal'],
        "titulo"=>"Consulta 8 con Active Record",
        "enunciado"=>"Listar todos los ciclistas que han ganado el maillot MGE (amarillo) en alguna etapa",
        "sql"=>"SELECT DISTINCT dorsal FROM etapa",
    ]);
}

public function actionConsulta8() {
    $dataProvider = new SqlDataProvider([
       'sql' => "SELECT DISTINCT dorsal FROM etapa", 
    ]);
    
    return $this->render("resultado",[
        "resultados"=>$dataProvider,
        "campos"=>['dorsal'],
        "titulo"=>"Consulta 8 con DAO",
        "enunciado"=>"Listar todos los ciclistas que han ganado el maillot MGE (amarillo) en alguna etapa",
        "sql"=>"SELECT DISTINCT dorsal FROM etapa",
    ]);
}

// -- (9) Listar el nombre de los puertos cuya altura sea mayor de 1500 
public function actionConsulta9a() {
    $dataProvider = new ActiveDataProvider([
        'query'=> Puerto::find()->select("nompuerto")->distinct()->where("altura>1500")
    ]);
            return $this->render("resultado",[
                "resultados"=>$dataProvider,
                "campos"=>['nompuerto'],
                "titulo"=>"Consulta 9 con Active Record",
                "enunciado"=>"Listar el nombre de los puertos cuya altura sea mayor de 1500",
                "sql"=>"SELECT DISTINCT nompuerto FROM puerto WHERE altura>1500"
            ]);
}

public function actionConsulta9() {
    $dataProvider = new SqlDataProvider([
        'sql'=> "SELECT DISTINCT nompuerto FROM puerto WHERE altura>1500"
    ]);
            return $this->render("resultado",[
                "resultados"=>$dataProvider,
                "campos"=>['nompuerto'],
                "titulo"=>"Consulta 9 con DAO",
                "enunciado"=>"Listar el nombre de los puertos cuya altura sea mayor de 1500",
                "sql"=>"SELECT DISTINCT nompuerto FROM puerto WHERE altura>1500"
            ]);
}

// -- (10) Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 o cuya altura esté entre 1800 y 3000
public function actionConsulta10a() {
    $dataProvider = new ActiveDataProvider([
       'query' => Puerto::find()->select("dorsal")->distinct()->where("pendiente>8 OR altura BETWEEN 1800 AND 3000") 
    ]);
    
    return $this->render("resultado",[
        "resultados"=>$dataProvider,
        "campos"=>['dorsal'],
        "titulo"=>"Consulta 10 con Active Record",
        "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 o cuya altura esté entre 1800 y 3000
",
        "sql"=>"SELECT DISTINCT dorsal FROM puerto WHERE pendiente>8 OR altura BETWEEN 1800 AND 3000"
    ]);
}

public function actionConsulta10() {
    $dataProvider = new SqlDataProvider([
       'sql' => "SELECT DISTINCT dorsal FROM puerto WHERE pendiente>8 OR altura BETWEEN 1800 AND 3000" 
    ]);
    
    return $this->render("resultado",[
        "resultados"=>$dataProvider,
        "campos"=>['dorsal'],
        "titulo"=>"Consulta 10 con DAO",
        "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 o cuya altura esté entre 1800 y 3000",
        "sql"=>"SELECT DISTINCT dorsal FROM puerto WHERE pendiente>8 OR altura BETWEEN 1800 AND 3000"
    ]);
}


// -- (11) Listar el dorsal de los ciclistas que hayan ganado algún puerto cuya pendiente sea mayor que 8 y cuya altura esté entre 1800 y 3000
public function actionConsulta11a() {
    $dataProvider = new ActiveDataProvider([
       'query' => Puerto::find()->select("dorsal")->distinct()->where("pendiente>8 AND altura BETWEEN 1800 AND 3000") 
    ]);
    
    return $this->render("resultado",[
        "resultados"=>$dataProvider,
        "campos"=>['dorsal'],
        "titulo"=>"Consulta 11 con Active Record",
        "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 y cuya altura esté entre 1800 y 3000
",
        "sql"=>"SELECT DISTINCT dorsal FROM puerto WHERE pendiente>8 AND altura BETWEEN 1800 AND 3000"
    ]);
}

public function actionConsulta11() {
    $dataProvider = new SqlDataProvider([
       'sql' => "SELECT DISTINCT dorsal FROM puerto WHERE pendiente>8 AND altura BETWEEN 1800 AND 3000" 
    ]);
    
    return $this->render("resultado",[
        "resultados"=>$dataProvider,
        "campos"=>['dorsal'],
        "titulo"=>"Consulta 11 con DAO",
        "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 y cuya altura esté entre 1800 y 3000",
        "sql"=>"SELECT DISTINCT dorsal FROM puerto WHERE pendiente>8 AND altura BETWEEN 1800 AND 3000"
    ]);
}


}
