<?php

namespace app\commands;

use yii\console\Controller;
use GuzzleHttp\Client;
use Symfony\Component\DomCrawler\Crawler;

class WikipediaController extends Controller
{
    public function actionGetElevation($cityName)
    {
        try {
            // Step 1: Get the Wikipedia page for the city
            $cityPageTitle = "{$cityName}, Spain";  // Adjust if needed
            $cityPageUrl = "https://en.wikipedia.org/wiki/" . urlencode($cityPageTitle);

            $client = new Client();
            $response = $client->request('GET', $cityPageUrl);

            if ($response->getStatusCode() !== 200) {
                throw new \Exception('Failed to fetch Wikipedia page.');
            }

            // Step 2: Use Symfony DomCrawler to extract elevation
            $crawler = new Crawler($response->getBody()->getContents());

            $elevation = $crawler->filter('th:contains("Elevation") + td')->text();

            if (empty($elevation)) {
                throw new \Exception('Elevation information not found.');
            }

            echo "Elevation of {$cityName}: {$elevation}\n";
        } catch (\Exception $e) {
            echo 'Error: ' . $e->getMessage() . "\n";
        }
    }
}
